json.array!(@commments) do |commment|
  json.extract! commment, :id, :post_id, :body
  json.url commment_url(commment, format: :json)
end
