class CreateCommments < ActiveRecord::Migration
  def change
    create_table :commments do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
